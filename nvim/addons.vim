colorscheme onedark

" vim airlines set up
let g:airline_powerline_fonts = 1   
" auto set setting
let g:auto_save = 1
let g:auto_save_events = ["InsertLeave", "TextChangedI"]

let g:onedark_config = {
    \ 'style': 'darker',
\}


" nerdtree set up 
let g:NERDTreeShowHidden = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeGitStatusUntrackedFilesMode = 'all'
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }

call coc#add_extension(
  \ 'coc-rust-analyzer',
  \ 'coc-git',
  \ 'coc-json',
  \ 'coc-highlight',
  \ 'coc-diagnostic',
\ )

lua << END
require('lualine').setup
{
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
    globalstatus = false,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'filename'},
    lualine_c = {{'diagnostics', sources = {'coc'}}},
    lualine_y = {'encoding', 'fileformat', 'filetype'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
}
END
