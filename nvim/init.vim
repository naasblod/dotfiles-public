source ~/.config/nvim/plugins.vim  

let g:loaded_node_provider=0
let g:loaded_ruby_provider=0
let g:loaded_perl_provider=0

" vim basic config

" speed
set updatetime=300

set modifiable 
set number
set nocompatible
set expandtab
set autoindent
set shiftwidth=2
set mouse=a
syntax on
set cursorline
set encoding=UTF-8
set background=dark
set splitbelow
set splitright
set noswapfile
set termguicolors
set t_BE=
set clipboard+=unnamedplus
colorscheme onedark

" add mappings
source ~/.config/nvim/mappings.vim

" add addtional setting 
source ~/.config/nvim/addons.vim

