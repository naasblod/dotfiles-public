nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gv :vsp<CR><Plug>(coc-definition)
nmap <silent> gh :sp<CR><Plug>(coc-definition)
nmap <silent> gr  <Plug>(coc-fix-current)
nmap <silent> gD :sp<CR><Plug>(coc-docs)
nmap <silent> <C-f> :GFiles<CR>
