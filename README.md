# dotfiles


configure the name of your personal and work directory in `install.sh`
change the `.gitconfig` file in `./personal` and `./work`
look over `./brew/Brewfile` and change it to suit your needs.
look over `./install.sh` so that you understand that it does not do anything odd and remove anything that you do not want it to do (if you want only the nvim configuration I would recommend you to comment everything else out.).

run `install.sh`

sets up two directories `$HOME/personal` and `$HOME/work` where different git configurations are used. 

this repo does not install Homebrew for you, but there is a line to uncomment in `install.sh` that will install Homebrew for you.

installs a bunch of software defined in `brew/Brewfile`.

for autocomplete with coc in nvim run `:PlugInstall` maybe.
might also need to use `:CocInstall` for language support. 

`nvim/mappings.vim`

```
nmap <silent> gd <Plug>(coc-definition)                 // go to definition
nmap <silent> gv :vsp<CR><Plug>(coc-definition)         // go to definition in a vertical split
nmap <silent> gh :sp<CR><Plug>(coc-definition)          // go to definition in a horizontal split
nmap <silent> gr  <Plug>(coc-fix-current)               // quick fix under cursor
nmap <silent> gD :sp<CR><Plug>(coc-docs)                // should go to docs but might not work
nmap <silent> <C-f> :GFiles<CR>                         // ctrl + f open fuzzy finder in git files
```

if you need any help, find me on discord.
