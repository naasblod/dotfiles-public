#!/bin/bash

DIR=${PWD}

echo "$DIR"

mkdir -p ~/git/personal
mkdir -p ~/git/work


# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew analytics off
brew bundle --file=brew/Brewfile
brew analytics off


ln -sFv "$DIR/git/work/.gitconfig" ~/git/work
ln -sFv "$DIR/git/personal/.gitconfig" ~

ln -sFv "$DIR/runcom/.bash_profile" ~
ln -sFv "$DIR/runcom/.bashrc" ~
ln -sFv "$DIR/runcom/.zshrc" ~
ln -sFv "$DIR/runcom/.inputrc" ~
ln -sFv "$DIR/.alias" ~
ln -sFv "$DIR/.functions" ~
ln -sFv "$DIR/kitty/" ~/.config


#neovim vim-plug
sh -c 'curl -sfLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'


ln -sFv "$DIR/nvim" ~/.config
