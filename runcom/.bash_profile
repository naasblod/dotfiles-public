#!/bin/bash

# Load .bashrc, which loads: ~/.{alias,functions}
if [[ -r "${HOME}/.bashrc" ]]; then
	# shellcheck source=/dev/null
	source "${HOME}/.bashrc"
fi

. "$HOME/.cargo/env"
